#!/usr/bin/Rscript
options(crayon.enabled=FALSE)
library(tidyverse)
library(patchwork)
library(arrow)
library(Rcpp) 

list.files("../data/traces_samplesize-50/", full.name=TRUE) %>% 
as_tibble() %>% 
mutate(config=gsub(".gz.parquet", "", gsub(".*/", "", value))) %>% 
#print 
mutate(trace=map(value, read_parquet)) %>% 
select(-value) %>% 
unnest(trace) -> traces

traces %>% 
select(config, phase, Operation, Start, End, group) %>% 
filter(Operation=="run_group") %>% 
group_by(config, phase) %>% 
summarize(start=min(Start), end=max(End)) %>% 
mutate(duration=end-start) %>% 
group_by(config) %>% 
summarize(makespan=sum(duration)) %>% 
ungroup() %>% 
arrange(makespan) %>% 
mutate(order=1:n()) %>% 
print -> traces.makespan

traces %>% 
select(config, Operation, group) %>% 
filter(Operation=="run_group") %>% 
distinct() %>% 
group_by(config) %>% 
count %>% 
rename(groupsn=n) %>% 
left_join(traces.makespan, by = "config") -> traces.groups

read_parquet("../data/MARE2DEM_refgroups.parquet") -> metrics

library(predictMARE)
emdata <- load_emdata()

traces.makespan %>% 
  ## mutate(groups.data=map(config, ~create_ref_groups(emdata, .x))) %>% 
  ## unnest(groups.data) %>% 
  ## select(config, makespan, order, group=RefGroup, Tx, Rx) %>% 
  ## group_by(config, makespan, order, group) %>% 
  ## summarize(nTx=sum(Tx), nRx=sum(Rx)) %>% 

  left_join(metrics, by = "config") %>% 
  group_by(config, makespan, order) %>% 
  summarize(
    ntx.mean=mean(nTx), 
    ntx.sd=sd(nTx), 
    ntx.q25 = quantile(nTx, probs=0.25)[[1]],
    ntx.q50 = quantile(nTx, probs=0.5)[[1]],
    ntx.q75 = quantile(nTx, probs=0.75)[[1]],


    nrx.mean=mean(nRx), 
    nrx.sd=sd(nRx), 
    nrx.q25 = quantile(nRx, probs=0.25)[[1]],
    nrx.q50 = quantile(nRx, probs=0.5)[[1]],
    nrx.q75 = quantile(nRx, probs=0.75)[[1]]

  ) -> tmp 

##   distinct() %>% 
##   filter(FreqId==1) %>% 
##   group_by(config, FreqId) %>% 
##   arrange(group) %>% 
##   mutate(group.infq=1:n()) -> freqs

## freqs %>% 
## group_by(config, FreqId) %>% 
## count %>% 
## rename(groupsn=n) -> freqs.countgroups

## #create_ref_groups(emdata,  "1-1-1") %>% group_by(Fq) %>% count

## metrics %>% 
## right_join(freqs) %>% 
## right_join(freqs.countgroups) %>% 
## group_by(order, config, groupsn, group, group.infq) %>% 
## summarize(pairs=sum(pairs)) %>% 
## mutate(width=1/(groupsn), x.pos=group.infq*width) %>% 
## mutate(start=x.pos-(width), end=x.pos) %>% 
## #mutate(pairs.perc=pairs/8527) -> fig.data
## mutate(pairs.perc=pairs/2040) -> fig.data

bind_rows(
tmp %>% 
select(config, makespan, order, mean=ntx.mean, sd=ntx.sd, q25=ntx.q25, q75=ntx.q75) %>% 
mutate(type="Tx"), 

tmp %>% 
select(config, makespan, order, mean=nrx.mean, sd=nrx.sd, q25=nrx.q25, q75=nrx.q75) %>% 
mutate(type="Rx")
) %>% 
  ggplot(aes(x=order, y=mean))+
#  ggplot(aes(x=order, y=pairs.q50))+
#   geom_errorbar(aes(ymin = mean - sd,
#                     ymax = mean + sd), size=.2) +

  geom_errorbar(aes(ymin = q25,
                    ymax = q75), size=.2) +

#  geom_ribbon(aes(ymin = pairs.min,
#                    ymax = pairs.max), alpha=.2) +

  geom_line(color="red", size=.7) + 
#  geom_smooth(span=.1, alpha=0)+
  geom_point()+

#  labs(x="x",y="y")+
  theme_bw(base_size=16)+
  facet_wrap(~type, scale="free_y")+
  labs(x="Ordem das configurações pelo tempo de execução", y="Média de elementos CSEM\nem cada grupo de refinamento") + 
theme(
    plot.margin = unit(c(0, 0, 0, 0), "cm"),
    legend.spacing = unit(1, "mm"),
#    panel.grid = element_blank(),
    legend.position = "top",
    legend.justification = "left",
    legend.box.spacing = unit(0, "pt"),
    legend.box.margin = margin(0, 0, 0, 0)
) + 
ylim(0, NA) -> p
#  coord_cartesian(ylim=c(0, 10))

ggsave(
dpi = 100,
filename="fig_4.6.png",
plot = p,
units="px",
path = "../figures/",
width = 1100,
height = 600
)
