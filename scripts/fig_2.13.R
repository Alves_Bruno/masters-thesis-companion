#!/usr/bin/Rscript
options(crayon.enabled=FALSE)
library(tidyverse)
library(patchwork)
library(arrow)
library(Rcpp) 
library(viridis)

path <- normalizePath("../data/refinement_details")
list.files(path, recursive=TRUE, pattern="*.jacobian.tri.bin") %>% 
as_tibble() %>% 
mutate(config=gsub("/.*", "", value)) %>% 
mutate(fname=gsub(".*/", "", value)) %>% 
separate(
  col=fname, 
  into=c("A", "group", "subset", "refine.type", "mesh", "phase", "tri", "bin"), 
  convert=TRUE, 
  remove=FALSE
) %>% 
mutate(value = paste0(path, "/", value)) %>% 
select(config, group, subset, refine.type, mesh, phase, value) -> meshes.files

sourceCpp("../src/rdgrid/rdgrid.cpp")
sourceCpp("../src/read_bin_g.cpp")

read_mesh <- function(file, type) {
  if(file.exists(file)){

  if(type=="local"){

    read_mesh_bin_A(file) -> mesh
    colnames(mesh) <- c("RefineNum", "ep1.y", "ep1.z", "ep2.y", "ep2.z", "ep3.y", "ep3.z", "area", "cond", "marked")
    as_tibble(mesh) %>% 
    mutate(error=0) %>%
    mutate(triangle=1:n()) %>% 
    return()
  
    } else if(type=="error"){
    read_mesh_bin_B(file) -> mesh
    colnames(mesh) <- c("RefineNum", "ep1.y", "ep1.z", "ep2.y", "ep2.z", "ep3.y", "ep3.z", "error", "zero")
    as_tibble(mesh) %>% 
    select(-zero) %>% 
    mutate(area=0, cond=0, marked=0) %>%
    mutate(triangle=1:n()) %>% 
    return()
    }
  
  } else {
    print(paste0("ERROR reading ", file))
    return(tibble())
  }
}

parse_fname_and_read <- function(files){
   tibble(value=files) %>% 
   mutate(fname=gsub(".*/", "", value)) %>% 
   separate(
     col=fname, 
     into=c("a","Group","iSubset","type", "meshnumber", "phase", "b", "c"), 
     sep="\\.", 
     convert=TRUE
   ) %>% 
   select(-a,-b,-c) %>% 
   mutate(phase=as.factor(phase)) %>%
   mutate(mesh.data=map2(value, type, read_mesh)) %>% 
   select(-value)
}

read_csv("../data/emdata/rx.csv", progress=FALSE, show_col_types = FALSE) %>% 
  select(y=Inline, z=Z) %>% 
  mutate(id=1:n()) -> rx.pos

read_csv("../data/emdata/tx.csv", progress=FALSE, show_col_types = FALSE) %>% 
  select(y=Inline, z=Z) %>% 
  mutate(id=1:n()) -> tx.pos

#dir.create(paste0(path, "/grids"))

read_csv(
  "../data/1-1-1-refgroups.csv",
  progress=FALSE, 
  show_col_types = FALSE
) -> elemental.groups

formatter1000 <- function(){
  function(x)x/1000
}

adjust_mesh_to_plot <- function(mesh.in){

mesh.in %>% 
  unnest(mesh.data) %>% 
  group_by(Group, iSubset, type, meshnumber, phase, triangle, area, cond, marked, error) %>% 
  pivot_longer(
    cols = starts_with("ep"),
    names_to = "ep",
    values_to = "value"
  ) %>% 
  mutate(axis=gsub(".*\\.", "", ep)) %>%
  mutate(ep=gsub("\\..*", "", ep)) %>%
  group_by(Group, iSubset, type, meshnumber, phase, triangle, area, cond, marked, error, ep) %>% 
  pivot_wider(
    names_from = axis,
    values_from = value
  ) %>%
  ungroup() %>% 
  select(-ep) #%>%
#  mutate(area=log(area, base=2))
}

meshes.files %>% 
  select(-phase) %>% 
  rename(file=value) %>% 
  mutate(mesh=map(file, parse_fname_and_read)) %>% 
  mutate(mesh.plot.data=map(mesh, adjust_mesh_to_plot)) -> meshes

path <- normalizePath("../data/")

#tibble(value="/1-1-1/04Tx013.1.6.local.9.jacobian.tri.bin") %>% 
tibble(value="04Tx013.1.1.local.1.jacobian.tri.bin") %>% 
mutate(fname=gsub(".*/", "", value)) %>% 
separate(
  col=fname, 
  into=c("A", "group", "subset", "refine.type", "mesh", "phase", "tri", "bin"), 
  convert=TRUE, 
  remove=FALSE
) %>% 
mutate(value = paste0(path, "/", value)) %>% 
select(group, subset, value) -> mesh.file

mesh.file %>% 
  rename(file=value) %>% 
  mutate(mesh=map(file, parse_fname_and_read)) %>% 
  mutate(mesh.plot.data=map(mesh, adjust_mesh_to_plot)) %>% 
  select(-mesh) %>% 
  unnest(mesh.plot.data) -> plot.data

bind_rows(
plot.data %>% 
  filter(round(cond)==780) %>% 
  left_join(plot.data %>% filter(round(cond)==1424) %>% select(y, z, cond), by = c("y", "z")) %>% 
  filter(!is.na(cond.x), !is.na(cond.y)) 
,
plot.data %>% 
  filter(round(cond)==780) %>% 
  left_join(plot.data %>% filter(round(cond)==1423525120) %>% select(y, z, cond), by = c("y", "z")) %>% 
  filter(!is.na(cond.x), !is.na(cond.y)) 
) -> geom.inicial

tibble(
  round_cond=c(1423525120, 780, 1424),
  rho=c(1e12, 0.3, 1),
  name=c("Ar", "Água", "Subsolo marinho")
) -> rho.values

#+begin_src R :results output file graphics :file (concat "../../images/" (replace-regexp-in-string " " "_" (nth 4 (org-heading-components))) "") :exports both :width 900 :height 800 :session *R*
show_local <- function(meshes.in){
#meshes %>% 
meshes.in %>% 
#  unnest(mesh.plot.data) %>%
  filter(type=="local") %>% 
#  filter(type=="error") %>% 
#  filter(area>1e3, area<) %>% 
#  mutate(area=log(area)) %>% 
  ggplot() + 
  geom_polygon(
    aes(
      group = triangle, x = y, y = -z, fill=area
    ), alpha=.7, size=0.1, color="black") +

  ## geom_polygon(
  ##   data= . %>% filter(marked==1), 
  ##   aes(
  ##     group = triangle, x = y, y = -z, fill=area
  ##   ), alpha=1, size=.05, color="black") +

#  geom_point(data=tx.pos %>% filter(id==27), aes(x=y, y=-z))+
#  geom_point(data=rx.pos %>% filter(id==1), aes(x=y, y=-z), shape=24, size=3, stroke=1.5, color="white")+

#  scale_fill_gradientn(colours = terrain.colors(100)) +
#  scale_fill_binned(type = "viridis")+

scale_fill_gradientn(colours = turbo(20),
                       breaks = seq(0, 50e3, by = 5e3),
                       limits = c(0, 50e3),
                       labels = as.character(seq(0, 50e3, by = 5e3)/1000)) + 

  theme_bw(base_size=20) +
  theme(
    panel.grid.major.x = element_blank(),
    panel.grid.minor.x = element_blank(),
    panel.grid.major.y = element_blank(),
    panel.grid.minor.y = element_blank(), 

    plot.margin = unit(c(0, 0, 0, 0), "cm"),
    legend.spacing = unit(1, "mm"),
    panel.grid = element_blank(),
    legend.position = "top",
    legend.justification = "left",
    legend.box.spacing = unit(0, "pt"),
    legend.box.margin = margin(0, 0, 0, 0),

    legend.key.height= unit(5, 'mm'),
    legend.key.width= unit(25, 'mm')
  ) +
  facet_wrap(~meshnumber, ncol=4)#+
#  coord_cartesian(ylim=c(-2e3, 200), xlim=c(9.5e3, 12.5e3))
#  coord_cartesian(ylim=c(-700, -600), xlim=c(10.975e3, 11.025e3))
}

meshes %>% unnest(mesh.plot.data) -> m
m %>% 
   filter(meshnumber <=4) %>% 
   show_local() + 
   scale_y_continuous(labels=formatter1000(), breaks=c(0, -1e3, -2e3)) + 
   scale_x_continuous(labels=formatter1000()) + 
   labs(x="Largura [Km]", y="Profundidade [Km]", fill="Área do triângulo [Km]:") + 
   geom_point(data=rx.pos %>% filter(id==1), aes(x=y, y=-z), shape=24, size=3, stroke=1.5, color="white")+
   theme(axis.title.x=element_blank()) +
   coord_cartesian(ylim=c(-2e3, 200), xlim=c(9.5e3, 12.5e3)) -> a

m %>% 
  filter(meshnumber > 4) %>% 
  show_local() + 
  scale_y_continuous(labels=formatter1000()) + 
  scale_x_continuous(labels=formatter1000(), breaks=c(10.98e3, 11e3, 11.02e3)) + 
  labs(x="Largura [Km]", y="Profundidade [m]") + 
  geom_point(data=rx.pos %>% filter(id==1), aes(x=y, y=-z), shape=24, size=1, stroke=1.5, color="white")+
  coord_cartesian(ylim=c(-700, -600), xlim=c(10.975e3, 11.025e3)) + 
  theme(legend.position="none") -> b


a + b + plot_layout(heights=c(1, 2)) -> p 

ggsave(
dpi = 100,
filename="fig_2.13.png",
plot = p,
units="px",
path = "../figures/",
width = 1100,
height = 1000
)
