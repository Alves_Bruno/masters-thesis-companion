#!/usr/bin/Rscript
options(crayon.enabled=FALSE)
library(tidyverse)
library(patchwork)
library(arrow)
library(Rcpp) 
library(predictMARE)

list.files("../data/traces_samplesize-50/", full.names=TRUE) %>% 
as_tibble() %>% 
mutate(trace=map(value, read_parquet)) %>% 
mutate(config=gsub(".gz.parquet", "", gsub(".*/", "", value))) %>% 
select(-value) %>% 
unnest(trace) -> traces

list.files("../data/composition", full.names=TRUE) %>% 
as_tibble() %>% 
mutate(comp=map(value, read_parquet)) %>% 
#mutate(config=gsub(".parquet", "", gsub(".*/", "", value))) %>% 
select(-value) %>% 
unnest(comp) -> comp

traces %>% 
select(config) %>% 
distinct() %>% 
mutate(groups=map(config, ~create_ref_groups(load_emdata(), .x))) %>% 
unnest(groups) -> config.groups

config.groups %>% select(config, group=RefGroup, fq=Fq) %>% distinct() -> fq.id

read_parquet("../data/MARE2DEM_refgroups.parquet") -> metrics

comp %>% 
  rename(group=in.group) %>%
  filter(ref_type=="error") %>% 
  group_by(config, phase, group, subset) %>% 
  arrange(mesh) %>% 
  slice_tail(n=1) %>% 
  select(config, phase, group, subset, depth) %>% 
#  print 
  slice(rep(1:n(), each = 4)) %>% 
  group_by(config, phase, group, subset) %>% 
  mutate(i=1:n()) %>% 
  mutate(subset=subset+i) %>% 
  select(-i) %>% 
  mutate(op="non.local.groups", mesh=0) -> adjust.1

comp %>% 
  rename(group=in.group) %>%
  filter(ref_type=="local") %>% 
  group_by(config, phase, group, subset) %>% 
  mutate(mesh.max=n()) %>% 

  arrange(depth) %>% 
  mutate(mesh=1:n()) %>%

  mutate(op=ifelse(mesh==1, "local.first", "local.middle")) %>% 
  mutate(op=ifelse(mesh==mesh.max, "local.last", op)) %>% 
  select(-mesh.max) %>% 
  ungroup() %>% 
  select(config, phase, op, group, mesh, depth) -> adjust.2

comp %>% 
  rename(group=in.group) %>%
  filter(ref_type=="error") %>% 
  mutate(op="error_estimate") %>% 
  select(config, phase, op, group, subset, mesh, depth) -> adjust.3

adjust.3 %>% 
  select(config, phase, group, subset) %>% 
  distinct() %>% 
  left_join(adjust.2, by = c("config", "phase", "group")) -> adjust.2.final

bind_rows(
adjust.2 %>% 
  filter(op=="local.last") %>% 
  mutate(op="em_primal") %>%
  slice(rep(1:n(), each = 6)) %>% 
  group_by(config, phase, group) %>% 
  mutate(i=1:n()) %>% 
  mutate(subset=(5*i) - 4) %>% 
  select(-i), 

adjust.3 %>% 
  mutate(op="em_primal")

) %>% bind_rows(

adjust.1 %>% 
  mutate(op="em_primal")

) %>% 
 arrange(config, phase, op, group, subset, depth) -> em.primal

em.primal

bind_rows(

adjust.1 %>% 
  filter(phase=="jacobian") %>% 
  mutate(op="em_derivs"),

adjust.3 %>% 
  filter(phase=="jacobian") %>% 
  group_by(config, phase, group, subset) %>% 
  arrange(mesh) %>% 
  slice_tail(n=1) %>% 
  mutate(op="em_derivs")
) -> em.derivs

em.derivs

bind_rows( adjust.2.final, adjust.3) %>% 
bind_rows( em.primal ) %>% 
bind_rows( em.derivs ) %>% 
left_join( metrics, by = c("config", "group")) -> predict.data

#rm(comp)
rm(adjust.1) 
rm(adjust.2)
rm(adjust.2.final)
rm(adjust.3) 
rm(em.primal)
rm(em.derivs)
gc()

predict.data %>% 
  filter(op=="error_estimate") %>% 
  select(config, phase, group, subset, mesh, depth) %>% 
  mutate(
    subset=as.integer(subset), 
    depth.est=as.integer(depth)
  ) %>% select(-depth) %>% 
  group_by(config, phase, group, subset) %>% 
  arrange(-mesh) %>% 
  slice_head(n=1) %>% 
  select(-mesh) -> a 

traces %>% 
  filter(Operation=="error_estimate") %>% 
  select(config, phase, group, subset, mesh, depth.in) %>% 
  rename(depth.real=depth.in) %>% 
  group_by(config, phase, group, subset) %>% 
  arrange(-mesh) %>% 
  slice_head(n=1) %>% 
  select(-mesh) -> b

b %>% 
  left_join(a) -> errorref.comparison

to_10000 <- function(){
  function(x) x/1e4
}

tibble(
  freq.value=c(0.125, 0.25, 0.50, 0.75, 1.0 , 1.25), 
  fq=1:6
) -> freq.values


max.value <- max(errorref.comparison$depth.real)

errorref.comparison %>% 

 left_join(fq.id) %>% 
# left_join(metrics, by = c("config", "group")) %>% 
# left_join(pairs.dist, by = c("config", "group")) %>%
#  filter(fq==1) %>% 
#  filter(mesh < 10) %>% 

#  filter(subset==26) %>% 

#  filter(config=="5-1-1") %>% 
  print -> tmp 

tmp %>% 
  group_by(subset, fq, depth.real) %>% 

  filter(subset==1, fq==1) %>% 
  summarize(y.max=max(depth.est), y.min=min(depth.est), n=n()) -> tmp2

tibble(
  start=seq(0, 200, 20), 
  end=seq(20, 220, 20)) %>% 
  mutate(pairs.group=sprintf("%.3d-%.3d", start, end)) %>% 
  mutate(group.id=1:n()) %>% 
  setDT() -> pairs.groups

tmp %>% 

  left_join(metrics) %>% 
  mutate(P=nRx+nTx) %>%
#  filter(subset==16) %>% 

  left_join(freq.values) %>% 
  mutate(freq.value=sprintf("%.3f Hz", freq.value)) %>%
  mutate(subset=sprintf("subset %.2d", subset)) %>% 
 
#  filter(phase=="jacobian") %>% 
#  filter(phase=="jacobian") %>% 

  ggplot()+
#  facet_grid(phase~group.id) + 
#  facet_grid(fq~group.id) + 
  facet_grid(subset~freq.value) + 
#  facet_grid(subset~fq)+ 
#  facet_grid(~subset, scale="free_x")+ 
  geom_point(aes(x=depth.real,y=depth.est), alpha=.07)+
  geom_smooth(aes(x=depth.real,y=depth.est), alpha=0, method="lm")+
  scale_x_continuous(labels=to_10000()) +
  scale_y_continuous(labels=to_10000()) +
#  scale_color_brewer(palette = "Set1") + 
#  scale_colour_viridis_c(option = "H") + 
  geom_segment(data=tibble(phase=c("jacobian", "smoothing")), aes(x=0, xend=max.value, y=0, yend=max.value), size=1, color="red3", alpha=.7)+
  labs(x="Triângulos na malha [x10000]", y="Estimativa [x10000]") + 
  theme_bw(base_size=16) -> p 

ggsave(
dpi = 100,
filename="fig_7.3.png",
plot = p,
units="px",
path = "../figures/",
width = 1100,
height = 850
)
