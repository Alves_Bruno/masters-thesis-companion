#!/usr/bin/Rscript
options(crayon.enabled=FALSE)
library(tidyverse)
library(arrow)

list.files("../data/traces_samplesize-50/", full.names=TRUE) %>% 
as_tibble() %>% 
mutate(trace=map(value, read_parquet)) %>% 
mutate(config=gsub(".gz.parquet", "", gsub(".*/", "", value))) %>% 
select(-value) %>% 
unnest(trace) -> traces
traces

traces %>% 
select(config, phase, Operation, Start, End, group) %>% 
filter(Operation=="run_group") %>% 
group_by(config, phase) %>% 
summarize(start=min(Start), end=max(End)) %>% 
mutate(duration=end-start) %>% 
group_by(config) %>% 
summarize(makespan=sum(duration)) %>% 
ungroup() %>% 
arrange(makespan) %>% 
mutate(order=1:n()) %>% 
print -> traces.makespan

to_hour <- function(){
  function(x) ifelse(as.double(x/3600) == round(x/3600), sprintf("%d", round(x/3600)), sprintf("%.1f", x/3600))
}

faster <- min(traces.makespan$makespan)
slower <- max(traces.makespan$makespan)

traces.makespan %>% 
  ggplot(aes(x=order,y=makespan))+
  geom_point()+
  scale_x_continuous(breaks=traces.makespan$order, labels=traces.makespan$config) +
#  scale_y_continuous(breaks=c(0, seq(0, 3600 * 8.5, 3600), slower, 4 * 3600), labels=to_hour()) +
  scale_y_continuous(breaks=seq(0, 3600 * 10, 3600), limits=c(0, 4*3600), labels=to_hour()) +
  theme_bw(base_size=16)+
  labs(x="Configurações ordenadas pelo tempo de execução", y="Makespan\n[Hora]")+
  theme(
    # Our theme 
    plot.margin = unit(c(0, 0, 0, 0), "cm"),
    legend.spacing = unit(1, "mm"),
    #panel.grid = element_blank(),
    legend.position = "top",
    legend.justification = "left",
    legend.box.spacing = unit(0, "pt"),
    legend.box.margin = margin(0, 0, 0, 0),
    
    panel.grid.minor.x = element_blank(),
    panel.grid.minor.y = element_blank(),
    axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)
   ) -> p

ggsave(
  dpi = 100,
  filename="fig_1.1.png",
  plot = p,
  units="px",
  path = "../figures/",
  width = 800,
  height = 300
)
