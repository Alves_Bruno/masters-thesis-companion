#!/usr/bin/Rscript
options(crayon.enabled=FALSE)
library(tidyverse)
library(patchwork)
library(arrow)
library(Rcpp) 

read_mesh <- function(file) {
  if(file.exists(file)){
  read_mesh_bin(file) -> mesh
  colnames(mesh) <- c("RefineNum", "ep1.y", "ep1.z", "ep2.y", "ep2.z", "ep3.y", "ep3.z", "area", "cond", "marked")
  as_tibble(mesh) %>% 
  mutate(triangle=1:n()) %>% 
  return()
  } else {
    print(paste0("ERROR reading ", file))
    return(tibble())
  }
}

parse_fname_and_read <- function(files){
   tibble(value=files) %>% 
   mutate(fname=gsub(".*/", "", value)) %>% 
   separate(
     col=fname, 
     into=c("a","Group","iSubset","type", "meshnumber", "phase", "b", "c"), 
     sep="\\.", 
     convert=TRUE
   ) %>% 
   select(-a,-b,-c) %>% 
   mutate(phase=as.factor(phase)) %>%
   mutate(mesh.data=map(value, ~read_mesh(.))) %>% 
   select(-value)
}


path <- normalizePath("../data/")

#tibble(value="/1-1-1/04Tx013.1.6.local.9.jacobian.tri.bin") %>% 
tibble(value="04Tx013.1.1.local.1.jacobian.tri.bin") %>% 
mutate(fname=gsub(".*/", "", value)) %>% 
separate(
  col=fname, 
  into=c("A", "group", "subset", "refine.type", "mesh", "phase", "tri", "bin"), 
  convert=TRUE, 
  remove=FALSE
) %>% 
mutate(value = paste0(path, "/", value)) %>% 
select(group, subset, value) -> mesh.file

sourceCpp("../src/rdgrid/rdgrid.cpp")
sourceCpp("../src/read_bin_g.cpp")

read_csv("../data/emdata/rx.csv", progress=FALSE, show_col_types = FALSE) %>% 
  select(y=Inline, z=Z) %>% 
  mutate(id=1:n()) -> rx.pos

read_csv("../data/emdata/tx.csv", progress=FALSE, show_col_types = FALSE) %>% 
  select(y=Inline, z=Z) %>% 
  mutate(id=1:n()) -> tx.pos

adjust_mesh_to_plot <- function(mesh.in){

mesh.in %>% 
  unnest(mesh.data) %>% 
  group_by(Group, iSubset, type, meshnumber, phase, triangle, area, cond, marked) %>% 
  pivot_longer(
    cols = starts_with("ep"),
    names_to = "ep",
    values_to = "value"
  ) %>% 
  mutate(axis=gsub(".*\\.", "", ep)) %>%
  mutate(ep=gsub("\\..*", "", ep)) %>%
  group_by(Group, iSubset, type, meshnumber, phase, triangle, area, cond, marked, ep) %>% 
  pivot_wider(
    names_from = axis,
    values_from = value
  ) %>%
  ungroup() %>% 
  select(-ep) %>%
  mutate(area=log(area, base=2))
}

mesh.file %>% 
  rename(file=value) %>% 
  mutate(mesh=map(file, parse_fname_and_read)) %>% 
  mutate(mesh.plot.data=map(mesh, adjust_mesh_to_plot)) %>% 
  select(-mesh) %>% 
  unnest(mesh.plot.data) -> plot.data

bind_rows(
plot.data %>% 
  filter(round(cond)==780) %>% 
  left_join(plot.data %>% filter(round(cond)==1424) %>% select(y, z, cond), by = c("y", "z")) %>% 
  filter(!is.na(cond.x), !is.na(cond.y)) 
,
plot.data %>% 
  filter(round(cond)==780) %>% 
  left_join(plot.data %>% filter(round(cond)==1423525120) %>% select(y, z, cond), by = c("y", "z")) %>% 
  filter(!is.na(cond.x), !is.na(cond.y)) 
) -> plot.data2

formatter1000 <- function(){
  function(x)x/1000
}

tibble(
  round_cond=c(1423525120, 780, 1424),
  rho=c(1e12, 0.3, 1),
  name=c("Ar", "Água", "Subsolo marinho")
) -> rho.values

plot.data2 %>%
#  filter(round(cond) %in% c(780, 1424)) %>% 
  ggplot() + 

  geom_polygon(
    data=plot.data %>% mutate(round_cond=round(cond)) %>% left_join(rho.values, by = "round_cond"), 
    aes(
      group=triangle, x = y, y = -z, fill=as.factor(name)
   ), alpha=0.5, size=0, color="black") +
  scale_fill_manual(breaks = c("Ar", "Água", "Subsolo marinho"), 
                       values=c("yellow1", "blue2", "chocolate4")) + 

  geom_line(data=.%>% filter(-z>-400), aes(x=y, y=-z), size=1)+
  geom_line(data=.%>% filter(-z<=-400), aes(x=y, y=-z), size=1)+

  geom_rect(
    data=tibble(), 
    aes(
      ymin=-1.8e3, ymax=-200,
      xmin=1000, xmax=43e3
    ), fill="green", color="black", alpha=0
  )+
  scale_y_continuous(labels=formatter1000()) + 
  scale_x_continuous(labels=formatter1000()) + 
  labs(x="Largura [Km]", y="Profundidade [Km]", fill="Região:") + 
  theme_bw(base_size=25) +
  theme(
    plot.margin = unit(c(0, 0, 0, 0), "cm"),
    legend.spacing = unit(1, "mm"),
    panel.grid = element_blank(),
    legend.position = "top",
    legend.justification = "left",
    legend.box.spacing = unit(0, "pt"),
    legend.box.margin = margin(0, 0, 0, 0)
    ) +
  coord_cartesian(ylim=c(-8e3, 1000), xlim=c(-85e3, 130e3)) -> geom.view

tibble(
  round_cond=c(1423525120, 780, 1424),
  rho=c(1e12, 0.3, 1),
  name=c("Ar", "Água", "Subsolo marinho")
) -> rho.values

plot.data2 %>%
#  filter(round(cond) %in% c(780, 1424)) %>% 
  ggplot() + 

  geom_polygon(
    data=plot.data %>% mutate(round_cond=round(cond)) %>% left_join(rho.values, by = "round_cond"), 
    aes(
      group=triangle, x = y, y = -z, fill=as.factor(name)
   ), alpha=0.5, size=0, color="black") +
  scale_fill_manual(breaks = c("Ar", "Água", "Subsolo marinho"), 
                       values=c("yellow1", "blue2", "chocolate4")) + 

  geom_line(data=.%>% filter(-z>-400), aes(x=y, y=-z), size=1)+
  geom_line(data=.%>% filter(-z<=-400), aes(x=y, y=-z), size=1)+

  geom_point(data=rx.pos, aes(x=y, y=-z), color="white", size=3, shape=17) + 
  geom_text(data=rx.pos %>% filter(id %in% c(1, seq(5, 20, 5))), aes(x=y, y=-z, label=id), color="black", size=6, nudge_y=-70) + 
  geom_point(data=tx.pos, aes(x=y, y=-z), color="white", size=1, shape=20) + 
  geom_text(data=tx.pos %>% filter(id %in% c(1, seq(25, 175, 25), 206)), aes(x=y, y=-z, label=id), color="black", size=6, nudge_y=100) + 

  scale_y_continuous(labels=formatter1000()) + 
  scale_x_continuous(labels=formatter1000()) + 
  labs(x="Largura [Km]", y="Profundidade [Km]", fill="Região:") + 
  theme_bw(base_size=25) +
  theme(
    plot.margin = unit(c(0, 0, 0, 0), "cm"),
    legend.spacing = unit(1, "mm"),
    panel.grid = element_blank(),
    legend.position = "top",
    legend.justification = "left",
    legend.box.spacing = unit(0, "pt"),
    legend.box.margin = margin(0, 0, 0, 0)
    ) +
  coord_cartesian(ylim=c(-1.8e3, -200), xlim=c(1000, 40.5e3)) -> geom.view.zoom

geom.view + facet_wrap(~"Geometria da região") + geom.view.zoom + facet_wrap(~"Zoom na área central") + theme(legend.position="none", axis.title.y=element_blank()) -> p 

ggsave(
  dpi = 100,
  filename="fig_2.5.png",
  plot = p,
  units="px",
  path = "../figures/",
  width = 1300,
  height = 600
)
