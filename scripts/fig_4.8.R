#!/usr/bin/Rscript
options(crayon.enabled=FALSE)
library(tidyverse)
library(patchwork)
library(arrow)
library(Rcpp) 

list.files("../data/traces_samplesize-50/", full.name=TRUE) %>% 
as_tibble() %>% 
mutate(config=gsub(".gz.parquet", "", gsub(".*/", "", value))) %>% 
#print 
mutate(trace=map(value, read_parquet)) %>% 
select(-value) %>% 
unnest(trace) -> traces

traces %>% 
select(config, phase, Operation, Start, End, group) %>% 
filter(Operation=="run_group") %>% 
group_by(config, phase) %>% 
summarize(start=min(Start), end=max(End)) %>% 
mutate(duration=end-start) %>% 
group_by(config) %>% 
summarize(makespan=sum(duration)) %>% 
ungroup() %>% 
arrange(makespan) %>% 
mutate(order=1:n()) %>% 
print -> traces.makespan

traces %>% 
select(config, Operation, group) %>% 
filter(Operation=="run_group") %>% 
distinct() %>% 
group_by(config) %>% 
count %>% 
rename(groupsn=n) -> traces.groups

traces %>% 
#select(Operation) %>% distinct()
filter(Operation %in% c("localRefinement", "error_estimate")) %>% 
select(config, Operation, group, subset, depth.in) %>% 
#filter(Operation == "error_estimate")
group_by(config, group, subset, Operation) %>% 
summarize(n.ref=n(), triangles=sum(depth.in)) %>% 
group_by(config, Operation) %>% 
summarize(
  triangles.mean=mean(triangles),
  triangles.sd=sd(triangles), 
  triangles.sum=sum(triangles), 
  
  nref.mean=mean(n.ref), 
  nref.sd=sd(n.ref),
  nref.sum=sum(n.ref)
) %>% 
left_join(traces.makespan) -> tmp

tmp

tibble(
  Operation=c("em2dkx", "em_primal", "em_derivs", "mpi_worker_run_subset", "error_estimate", "localRefinement"), 
  name=c("subset", "solve_primal", "adj_derivs", "Grupo de Refinamento", "estimate_error", "local_refinement")
) -> names.t

tmp %>% 
  left_join(traces.groups) %>% 
  left_join(names.t) %>% 
  ggplot(aes(x=groupsn,y=nref.sum))+
  geom_smooth(method="lm")+
  geom_point()+
  labs(x="config",y="y")+
  theme_bw(base_size=16)+
theme(
    plot.margin = unit(c(0, 0, 0, 0), "cm"),
    legend.spacing = unit(1, "mm"),
    legend.position = "top",
    legend.justification = "left",
    legend.box.spacing = unit(0, "pt"),
    legend.box.margin = margin(0, 0, 0, 0)
) + 
  ylim(0, NA) +
  labs(x="Ordem das configurações pelo tempo de execução", y="Passos de refinamento") + 
  facet_wrap(~name, scales="free_y") -> a

tmp %>% 
  left_join(traces.groups) %>% 
  left_join(names.t) %>% 
  filter(order != 9 ) %>% 
  ggplot(aes(x=groupsn,y=nref.sum))+
  geom_smooth(method="lm")+
  geom_point()+
  labs(x="config",y="y")+
  theme_bw(base_size=16)+
theme(
    plot.margin = unit(c(0, 0, 0, 0), "cm"),
    legend.spacing = unit(1, "mm"),
#    panel.grid = element_blank(),
    legend.position = "top",
    legend.justification = "left",
    legend.box.spacing = unit(0, "pt"),
    legend.box.margin = margin(0, 0, 0, 0)
) + 
  ylim(0, NA) +
#  geom_hline(aes(yintercept=0))+
  labs(x="Quantidade de grupos de refinamento", y="Passos de refinamento") + 
  facet_wrap(~name, scales="free_y") -> b

a + theme(axis.title.x=element_blank()) + b + plot_layout(ncol=1) -> p 

ggsave(
dpi = 100,
filename="fig_4.8.png",
plot = p,
units="px",
path = "../figures/",
width = 1100,
height = 600
)
