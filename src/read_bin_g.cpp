#include<iostream>
#include<fstream>
#include <Rcpp.h>

using namespace Rcpp;
using namespace std;

// [[Rcpp::export]]
NumericMatrix read_mesh_bin(String file) {

    FILE *f;
    f = fopen(file.get_cstring() , "r");
    fseek(f, 0, SEEK_END);
    unsigned long len = (unsigned long)ftell(f);
    fclose(f);

   ifstream rf(file.get_cstring(), ios::out | ios::binary);
   if(!rf) {
      cout << "Cannot open file!" << endl;
      return 1;
   }

   int nparams = 10;
   int intsize = 4;
   int ntri = len / nparams / intsize;
//   cout << "bin len: " << len << endl;
//   cout << "ntri: " << ntri << endl;
   float value[ntri*nparams];
   NumericMatrix df(ntri, nparams);
//   NumericMatrix df(1, nparams);
//   return(df);
   rf.read((char *) &value, ntri*nparams*intsize);

   for(int i = 0; i < ntri; i++){
     for(int j = 0; j < nparams; j++){
//       df[j * ntri + i] = value[i * nparams + j];
       df[i * nparams + j] = value[i * nparams + j];
     }
   }

   rf.close();
   return(df);
}

// [[Rcpp::export]]
NumericMatrix read_mesh_bin_A(String file) {

    FILE *f;
    f = fopen(file.get_cstring() , "r");
    fseek(f, 0, SEEK_END);
    unsigned long len = (unsigned long)ftell(f);
    fclose(f);

   ifstream rf(file.get_cstring(), ios::out | ios::binary);
   if(!rf) {
      cout << "Cannot open file!" << endl;
      return 1;
   }

   int nparams = 10;
   int intsize = 4;
   int ntri = len / nparams / intsize;
//   cout << "bin len: " << len << endl;
//   cout << "ntri: " << ntri << endl;
   float value[ntri*nparams];
   NumericMatrix df(ntri, nparams);
//   NumericMatrix df(1, nparams);
//   return(df);
   rf.read((char *) &value, ntri*nparams*intsize);

   for(int i = 0; i < ntri; i++){
     for(int j = 0; j < nparams; j++){
//       df[j * ntri + i] = value[i * nparams + j];
       df[i * nparams + j] = value[i * nparams + j];
     }
   }

   rf.close();
   return(df);
}

// [[Rcpp::export]]
NumericMatrix read_mesh_bin_B(String file) {

    FILE *f;
    f = fopen(file.get_cstring() , "r");
    fseek(f, 0, SEEK_END);
    unsigned long len = (unsigned long)ftell(f);
    fclose(f);

   ifstream rf(file.get_cstring(), ios::out | ios::binary);
   if(!rf) {
      cout << "Cannot open file!" << endl;
      return 1;
   }

   int nparams = 9;
   int intsize = 4;
   int ntri = len / nparams / intsize;
//   cout << "bin len: " << len << endl;
//   cout << "ntri: " << ntri << endl;
   float value[ntri*nparams];
   NumericMatrix df(ntri, nparams);
//   NumericMatrix df(1, nparams);
//   return(df);
   rf.read((char *) &value, ntri*nparams*intsize);

   for(int i = 0; i < ntri; i++){
     for(int j = 0; j < nparams; j++){
//       df[j * ntri + i] = value[i * nparams + j];
       df[i * nparams + j] = value[i * nparams + j];
     }
   }

   rf.close();
   return(df);
}
